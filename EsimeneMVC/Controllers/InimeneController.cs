﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EsimeneMVC.Models;

namespace EsimeneMVC.Controllers
{
    public class InimeneController : MyController
    {
        // GET: Inimene
        public ActionResult Index()         // List
        {
            

            return View(Inimene.Inimesed);
        }

        // GET: Inimene/Details/5
        public ActionResult Details(int id) // Read
        {
            Inimene x = Inimene.Find(id);
            if (x == null) return HttpNotFound();
            return View(x);
        }

        // GET: Inimene/Create              // Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inimene/Create
        [HttpPost]
        public ActionResult Create(Inimene inimene)
        {
            try
            {
                // TODO: Add insert logic here
                Inimene.Inimesed.Add(
                    //new Inimene
                    //{
                    //    Nimi = inimene.Nimi,
                    //    Vanus = inimene.Vanus
                    //}
                    inimene
                    );

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult VanaCreate(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                Inimene.Inimesed.Add(
                    new Inimene
                    {
                        Nimi = collection["Nimi"],
                        Vanus = int.Parse(collection["Vanus"])
                    }
                    );

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimene/Edit/5              // Update
        public ActionResult Edit(int id)
        {
            Inimene x = Inimene.Find(id);
            if (x == null) return HttpNotFound();
            return View(x);
        }

        // POST: Inimene/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Inimene inimene)
        {
            try
            {
                // TODO: Add update logic here
                
                
                    var x = Inimene.Find(id);
                    x.Nimi = inimene.Nimi;
                    x.Vanus = inimene.Vanus;
                

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimene/Delete/5            // Delete
        public ActionResult Delete(int id)
        {
            Inimene x = Inimene.Find(id);
            if (x == null) return HttpNotFound();
            return View(x);
        }

        // POST: Inimene/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Inimene x = Inimene.Find(id);
                // TODO: Add delete logic here
                if (x != null) Inimene.Inimesed.Remove(x);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
