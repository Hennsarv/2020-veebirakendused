﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EsimeneMVC.Controllers
{
    public class MyController : Controller
    {
        #region lisatud mugavuseks
        // TempBag on TempData paariline ja väärtustatakse konstruktoris
        protected dynamic TempBag;
        
        // SessionBag on Session-objekti paariline, seda ei ole konstruktori ajal veel olemas
        // Seepärast kasutan ma tema väärtustamiseks "esimese pöördumise" funktsiooni
        // kui private sessionBag on olemas, siis võtan selle, muidu tekitan ta
        dynamic sessionBag;
        protected dynamic SessionBag => sessionBag ?? (sessionBag = new UniversalBag(System.Web.HttpContext.Current.Session));

        // RequestBag on samasugune paariline Request.Sessioni jaoks
        dynamic paramBag;
        protected dynamic RequestBag => paramBag ?? (paramBag = new UniversalBag(Request));

        // AppBag Applicationi jaoks
        dynamic appBag;
        protected dynamic AppBag => appBag ?? (appBag = new UniversalBag(System.Web.HttpContext.Current.Application));

        // BagKookie ja CookieBag kahepeale toimetavad kookidega (abiks ka SetCookie) 
        // bag tehakse taas esimesel pöördumisel, kusjuures kahel moel
        // kui Requesti kookide hulgas on sellenimeline kooki olemas, võetakse sealt
        // kui ei ole, tehakse uus
        // CookieBag on siis selle kookie (sõnastikku) bag
        HttpCookie bagCookie;
        protected HttpCookie BagCookie =>
            bagCookie
                ?? (bagCookie = Request.Cookies["bagtest"])
                ?? (bagCookie = new HttpCookie("bagtest"))
            ;
        dynamic cookieBag;
        protected dynamic CookieBag => cookieBag ?? (cookieBag = new UniversalBag(BagCookie));
        
        // TempData on konstruktori ajal juba olemas, seepärast ei ole teda vaja 
        // sarnaselt eelnevatega teha
        public MyController()
        {
            TempBag = new UniversalBag(TempData);
        }

        // Kooki-dictionay sisse uute väärtuste pistmine käib pisut teisiti
        // seepärast on siin tehtud meetod selle jaoks
        // klient saadab oma kookied alati serveri poole
        // server saadab tagasi vaid need, mis muutuvad
        protected void SetCookie(string name, object value, int hours = 1)
        {
            if (value == null) return; // nullli ei hakka salvestama
            BagCookie.Values[name] = value.ToString(); // paneme kookie sisse uue value
            BagCookie.Expires = DateTime.Now.AddHours(hours); // määrame kookile uue aegumisaja
            Response.Cookies.Add(BagCookie); // lisame kookie response.cookies kollektsiooni
        }

        #endregion


        // GET: My
        public ActionResult Index(string nimi, string id)
        {
            // 1. ViewBag ja ViewData -- meeldetuletuseks

            ViewBag.TestViewBag = "selle teksti pistsin ViewBagi";
            ViewData["TestViewData"] = "selle teksti pistsin ViewDatasse";

            // 2. parameetrite katsetamine

            // 3. Mis vahe on ViewDatal ja TempDatal

            TempData["Nr"] = 77;
            TempBag.Nr++;

            // 4. Mis on Session ja mis on Application [ja Controller static]

            ViewBag.AppStarted = AppBag.Started;
            ViewBag.AppRequestNr = ++AppBag.RequestNr;

            SessionBag.RequestNr++;
            // Session["ReuestNr"] = (int)(Session["RequestNr"]) + 1;

            // 5. Mis asi on Cookie ja kuidas teda kasutada

            // kommentaarides on kood, kui neid bag-e ei kasuta
            if(RequestBag.Nimi != null) // if(Params["Nimi"] != null)
            {
                SetCookie("Nimi", RequestBag.Nimi);
                                // HttpCookie h = new HttpCookie("BagTest")
                                // h["Nimi"] = Params["Nimi"]
                                // h.Expires = DateTime.Now.AddHours(hours);
                                // Response.Cookies.add(h)
            }

            ViewBag.Nimi = CookieBag.Nimi;
                                // HttpCookie h = new HttpCookie["BagTest"]
                                // või Request.Cookies["BagTest"]
                                // ViewBag.Kook = h["Kook"]


            /* 
            return RedirectToAction("NoIndex");
            // */
            return View();
        }

        public ActionResult NoIndex()
        {
            return View("Index");
        }
    }

    public class UniversalBag : DynamicObject
    {
        dynamic bag;
        public UniversalBag() => bag = new Dictionary<string, dynamic>();
        public UniversalBag(dynamic bag) => this.bag = bag;

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            switch (bag)
            {
                case IDictionary<string, object> d:
                    result = d.ContainsKey(binder.Name) ? d[binder.Name] : null;
                    return true;
                default:
                    result = bag[binder.Name];
                    return true;
            }

        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            bag[binder.Name] = value;
            return true;
        }


    }



}