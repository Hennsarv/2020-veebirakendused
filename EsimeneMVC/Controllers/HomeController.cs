﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EsimeneMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public string Tere(string id = "tundmatu")
        {
            return $"Tere {id}";
        }

        public ActionResult Test()
        {
            return View();
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            ViewBag.Sõnum = "Täna saame õlutit";
            ViewData["TeineSõnum"] = "nb 17-18 on häppi hour";
            ViewData["hommikul kell 10"] = "äratus";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}