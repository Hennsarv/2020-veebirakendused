﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EsimeneMVC.Models;    // et saaks mudelit kasutada

namespace EsimeneMVC.Controllers
{
    public class StudentsController : Controller 
        // Õpilased ei saa kontrollerile nimeks panna, kuna URL ei luba Õ-d
    {
        // GET: Students
        public ActionResult Index()
        {
            return View(Õpilane.Õpilased);
        }

        // GET: Students/Details/5
        // kontroller (tegemise ajal) ei teadnud, mis tüüpi meie id olema hakkab
        // seepärast pani ta int kuigi meil on string (isikukood)
        public ActionResult Details(string id)
        {
            Õpilane õ = Õpilane.Find(id);
            if (õ == null) return HttpNotFound();
            return View(õ);
        }

        // GET: Students/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Students/Create
        [HttpPost]
        //public ActionResult Create(FormCollection collection)
        public ActionResult Create(Õpilane õpilane)
        {
            // ma ei mäleta, kuidas seda kontrollimist teha, et oleks kõik asjad olemas
            // õpilane.Add oli mul tehtud nii, et kui topelt isikukood, siis annab false
            // tegin kogu lisamise ümber ilma selle tobeda try-catchita
            if (õpilane.Add()) return RedirectToAction("Index");
            else
            {
                // nii saan ma View-le anda infot (lisaks mudelile)
                ViewBag.Error = "selline isikukood juba on"; // mõtleme siin mida teha
                // 
                return View(õpilane); // sel moel saadan ma täidetud vormi tuldud teed tagasi
            }
        }

        // GET: Students/Edit/5
        public ActionResult Edit(string id)
        {
            Õpilane õ = Õpilane.Find(id);
            if (õ == null) return HttpNotFound();
            return View(õ);
        }

        // POST: Students/Edit/5
        [HttpPost]
        public ActionResult Edit(string id, Õpilane õpilane)
        {
            Õpilane õ = Õpilane.Find(id);
            try
            {
                // TODO: Add update logic here
                if(õ != null)
                {
                    õ.Nimi = õpilane.Nimi;
                    õ.Klass = õpilane.Klass;
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View(õ);
            }
        }

        // GET: Students/Delete/5
        public ActionResult Delete(string id)
        {
            Õpilane õ = Õpilane.Find(id);
            if (õ == null) return HttpNotFound();
            return View(õ);
        }

        // POST: Students/Delete/5
        [HttpPost]
        public ActionResult Delete(string id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                Õpilane.Find(id)?.Delete();

                return RedirectToAction("Index");
            }
            catch
            {
                // igaks juhuks veatöötlus, aga see ei tohiks kunagi siia jõuda
                // see on kui postiga tuleb tagasi olemati isikukood
                return View(Õpilane.Find(id)); // las jookseb parem kinni
            }
        }
    }
}
