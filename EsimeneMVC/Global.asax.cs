﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace EsimeneMVC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        int sessionNr = 0;
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Application["RequestNr"] = 0;
            Application["Started"] = System.DateTime.Now;
        }

        protected void Session_Start()
        {
            Session["Nr"] = ++sessionNr;
            Session["RequestNr"] = 0;
            Session["Started"] = System.DateTime.Now;
        }

       
    }
}
