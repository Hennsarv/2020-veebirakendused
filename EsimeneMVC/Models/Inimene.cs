﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EsimeneMVC.Models
{
    public class Inimene
    {
        static int loendur = 0;

        public static List<Inimene> Inimesed { get; private set; }
            = new List<Inimene>()
            {
                new Inimene
                {
                    Nimi = "Henn",
                    Vanus = 64
                }
            }
            ;

        public static Inimene Find(int id) => 
            Inimesed.Where(x => x.Nr == id).SingleOrDefault();

        [Display(Name = "Number")]
        [Key] 
        public int Nr { get; private set; } = ++loendur;
        public string Nimi { get; set; }
        public int Vanus { get; set; }
    }
}