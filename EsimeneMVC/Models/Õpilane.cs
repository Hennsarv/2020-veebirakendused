﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
// selle rea lisan, et saaks neid nurksulgudes asju lisada fieldidele
using System.ComponentModel.DataAnnotations; // NB! Seda vaja kui [] asju kasutada

namespace EsimeneMVC.Models
{
    public class Õpilane
    {
        [Key]
        [Display(Name = "Isikukood")] // ta kurask arvab, et KEY tuleb endal leiutada
                                      // ma pidin view peal käsitööd tegema - see pole aus
        public string IK { get; set; } // NB! kõik asjad propertid, et see genereerija neid usuks
        // kahjuks ei saa IK-d read only teha, kuna siis ei saa seda klassi kontroller-view vahel probleemita kasutada
        public string Nimi { get; set; }
        public string Klass { get; set; }

        // siiani ok

        // see on dictionary, kus hakkan õpilaisi hoidma, las see olla privaatne
        private static Dictionary<string, Õpilane> _Õpilased = new Dictionary<string, Õpilane>();

        // see on avalik "List", mis sisaldab õpilaisi
        public static IEnumerable<Õpilane> Õpilased => _Õpilased.Values;

        // nüüd paar meetodit - selgitan ka
        // Add funktsioon lisab õpilase dictionary, kui teda seal ei ole
        public bool Add()
        {
            if (!_Õpilased.ContainsKey(this.IK))
            {
                _Õpilased.Add(IK, this);
                return true;
            }
            else return false;
        }

        // find funktsioon leiab õpilase dictionarist
        public static Õpilane Find(string ik)
            => _Õpilased.ContainsKey(ik) ? _Õpilased[ik] : null;

        // delete meetod kustutab ta dictionarist
        public void Delete()
        {
            if (_Õpilased.ContainsKey(this.IK)) _Õpilased.Remove(this.IK);
        }

        // neid funktsioone on vaja, kuna ma ei raatsi dictionary publikuks teha
    }
}