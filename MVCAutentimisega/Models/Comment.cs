//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVCAutentimisega.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Comment
    {
        public int Id { get; set; }
        public string Commenter { get; set; }
        public string Comment1 { get; set; }
        public System.DateTime Created { get; set; }
        public string DriverId { get; set; }
        public string ParticipantId { get; set; }
        public Nullable<int> CarId { get; set; }
        public Nullable<int> TripId { get; set; }
        public Nullable<int> Grade { get; set; }
    
        public virtual AspNetUser WhoCommented { get; set; }
        public virtual AspNetUser CommentedDriver { get; set; }
        public virtual AspNetUser CommentedParticipant { get; set; }
        public virtual Car Car { get; set; }
        public virtual Trip Trip { get; set; }
    }
}
