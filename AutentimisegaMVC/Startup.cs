﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AutentimisegaMVC.Startup))]
namespace AutentimisegaMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
