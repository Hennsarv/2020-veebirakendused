﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCrakendusDemo.Models;

namespace MVCrakendusDemo.Controllers
{
    public class AutodController : MyController
    {
        private CarPoolEntities db = new CarPoolEntities();

        // GET: Autod
        public ActionResult Index()
        {
            return View(db.Autoes.ToList());
        }

        // GET: Autod/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auto auto = db.Autoes.Find(id);
            if (auto == null)
            {
                return HttpNotFound();
            }
            return View(auto);
        }

        // GET: Autod/Create
        public ActionResult Create()
        {
            ViewBag.Tags = db.Tags.Select(x => x.Name).ToList();
            ViewBag.SelectedTags = "";
            return View();
        }

        // POST: Autod/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nimi")] Auto auto, string Nupp, string Tags, string NewTag)
        {
            var tags = (Tags+"").Split(',').Where(x => x != "").ToList();
            if (Nupp == "NewTag")
            {
                try
                {
                    NewTag = NewTag.Trim();
                    if (NewTag == "") throw new Exception();
                    db.Tags.Add(new Tag { Name = NewTag });
                    db.SaveChanges();
                    tags.Add(NewTag);
                }
                catch
                { }
            }
            else if (Nupp != "Save")
            {
                if (tags.Contains(Nupp)) tags.Remove(Nupp); else tags.Add(Nupp);
            }
            else if (ModelState.IsValid)
            {
                db.Autoes.Add(auto);
                db.SaveChanges();
                db.Tags.Where(x => tags.Contains(x.Name))
                    .ToList()
                    .ForEach(x => auto.Tags.Add(x))
                    ;
                db.SaveChanges();
 
                return RedirectToAction("Index");
            }
            ViewBag.SelectedTags = string.Join(",",tags);
            ViewBag.Tags = db.Tags.Select(x => x.Name).Where(x => x != "").ToList();
            return View(auto);
        }

        // GET: Autod/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auto auto = db.Autoes.Find(id);
            if (auto == null)
            {
                return HttpNotFound();
            }
            return View(auto);
        }

        // POST: Autod/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nimi")] Auto auto, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(auto).State = EntityState.Modified;
                db.SaveChanges();
                ChangeDataFile(file, x => { auto.DataFiles.Add(db.DataFiles.Find(x)); db.SaveChanges(); }, null);

                return RedirectToAction("Edit", new { id = auto.Id });
            }
            return View(auto);
        }

        public ActionResult RemovePicture(int id, int pictureId)
        {
            Auto auto = db.Autoes.Find(id);
            if(auto != null)
            {
                auto.DataFiles.Remove(db.DataFiles.Find(pictureId));
                db.DataFiles.Remove(db.DataFiles.Find(pictureId));
                db.SaveChanges();
            }

            return RedirectToAction("Edit", new { id });
        }
        // GET: Autod/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auto auto = db.Autoes.Find(id);
            if (auto == null)
            {
                return HttpNotFound();
            }
            return View(auto);
        }

        // POST: Autod/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Auto auto = db.Autoes.Find(id);
            db.Autoes.Remove(auto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
