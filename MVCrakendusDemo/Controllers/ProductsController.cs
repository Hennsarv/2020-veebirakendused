﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCrakendusDemo.Models;

namespace MVCrakendusDemo.Controllers
{
    public class ProductsController : Controller
    {
        private northwindEntities2 db = new northwindEntities2();

        // GET: Products
        public ActionResult Index(int? id, string order = "", string direction = "asc", int? editId = null)
        {
            var products = db.Products
                .Where(x => !id.HasValue || x.CategoryID == id.Value)
                .OrderBy(x => x.ProductID)
                ;
            IOrderedQueryable<Product> sproducts = null;
            
            switch(order+direction)
            {
                case "nameasc": sproducts = products.OrderBy(x => x.ProductName); break;
                case "namedesc": sproducts = products.OrderByDescending(x => x.ProductName); break;
                case "priceasc": sproducts = products.OrderBy(x => x.UnitPrice); break;
                case "pricedesc": sproducts = products.OrderByDescending(x => x.UnitPrice); break;
                default: sproducts = products.OrderBy(x => x.ProductID); break;
            }
                ;
            ViewBag.CatId = id;
            ViewBag.EditId = editId;

            ViewBag.Order = order;
            ViewBag.Direction = direction;

            ViewBag.CategoryName = id.HasValue
                ? db.Categories.Find(id.Value)?.CategoryName ?? ""
                : "Kõik tooted";
            return View(sproducts.ToList());
        }

        public ActionResult Like(int id)
        {
            Users u = db.Users.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            Product p = db.Products.Find(id);
            if (u != null && p != null)
            {
                try
                {
                    p.Likers.Add(u);
                    db.SaveChanges();
                }
                catch (Exception)
                {
                }
            }
            return RedirectToAction("Index");
        }
        public ActionResult DisLike(int id)
        {
            Users u = db.Users.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            Product p = db.Products.Find(id);
            if (u != null && p != null)
            {
                try
                {
                    p.Likers.Remove(u);
                    db.SaveChanges();
                }
                catch (Exception)
                {
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult AddComment(int id, string content)
        {
            Product p = db.Products.Find(id);
            Users u = db.Users.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            if (p != null && u != null)
            {
                p.ProductComments.Add(
                    new ProductComment
                    {
                        UserId = u.Id,
                        Created = DateTime.Now,
                        Content = content
                    }
                    );
                db.SaveChanges();

            }

            return RedirectToAction("Index");
        }
        public ActionResult RemoveComment(int id)
        {
            ProductComment c = db.ProductComments.Find(id);
            if (c != null)
            {
                db.ProductComments.Remove(c);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }


        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product products = db.Products.Find(id);
            if (products == null)
            {
                return HttpNotFound();
            }
            return View(products);
        }

        // GET: Products/Create
        public ActionResult Create(int? id)
        {
            ViewBag.CatId = id;
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", id);
            return View(new Product { CategoryID = id });
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductID,ProductName,SupplierID,CategoryID,QuantityPerUnit,UnitPrice,UnitsInStock,UnitsOnOrder,ReorderLevel,Discontinued")] Product product, int? catId)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();

                if (catId.HasValue)
                    return RedirectToAction("Details", "Categories", new { id = catId });
                else
                    return RedirectToAction("Index");
            }

            ViewBag.CatId = catId;
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", product.CategoryID);
            return View(product);
        }

        public ActionResult ChangeProduct (Product product, string order, string direction, int? catId)
        {
            db.Entry(product).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index", new { order, direction, catId });

        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id, int? catId)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.CatId = catId;
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", product.CategoryID);
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductID,ProductName,SupplierID,CategoryID,QuantityPerUnit,UnitPrice,UnitsInStock,UnitsOnOrder,ReorderLevel,Discontinued")] Product product, int? catId)
        {
            if (product.ProductName == null || product.ProductName == "")
                ModelState.AddModelError("ProductName", "tootel peab nimi olema");

            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                if (catId.HasValue)
                    return RedirectToAction("Details", "Categories", new { id = catId });
                else
                    return RedirectToAction("Index");
            }
            ViewBag.CatId = catId;
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", product.CategoryID);
            return View(product);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
