﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCrakendusDemo.Controllers
{
    
    public class KaartController : Controller
    {
        static int[] pakk;
        static bool[] face;
        static int[] kuhjad;
        // GET: Kaart
        public ActionResult Index(int? id, int? kuhi)
        {
            if (id.HasValue)
            {
                face[id.Value] = !face[id.Value];
                if (kuhi.HasValue)
                {
                    kuhjad[id.Value] = kuhi.Value;
                    id = null;
                }
            }
            else
            {
                Random r = new Random();
                int[] segatud = Enumerable.Range(1, 52)
                .Select(x => new { x, r = r.NextDouble() })
                .OrderBy(x => x.r)
                .Select(x => x.x)
                .ToArray();
                // näide kuidas mina teeks segatud kaardipaki

              
                pakk = segatud.Take(5).ToArray();
                face = pakk.Select(x => true).ToArray();
                kuhjad = pakk.Select(x => 0).ToArray();
            }
            ViewBag.Kaardid = pakk;
            ViewBag.Faces = face;
            ViewBag.Kuhjad = kuhjad;
            ViewBag.Nimed = new string[] { "Laud", "Ida", "Lääs" };
            ViewBag.Id = id;
            return View();
        }
    }
}