﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCrakendusDemo.Models;

namespace MVCrakendusDemo.Controllers
{
    [Authorize]
    public class CarsController : MyController
    {
        //private CarPoolEntities db = new CarPoolEntities();

        // GET: Cars
        public ActionResult Index()
        {
            var cars = db.Cars.Include(c => c.AspNetUser).Include(c => c.DataFile);
            return View(cars.ToList());
        }

        // GET: Cars/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = db.Cars.Find(id);
            if (car == null)
            {
                return HttpNotFound();
            }

            var juhid = car.Drivers.Select(x => x.Id).ToList();
            var mittejuhid = db
                .AspNetUsers
                .Where(x => !juhid.Contains(x.Id))
                .ToList();
            ViewBag.Mittejuhid = mittejuhid;
            return View(car);
        }

        // GET: Cars/Create
        public ActionResult Create()
        {
            ApplicationUser auser = ApplicationUser.GetUser(User.Identity.Name);
            ViewBag.Owner = new SelectList(db.AspNetUsers, "Id", "DisplayName", auser.Id);
            return View(new Car { Owner = auser.Id });
        }

        // POST: Cars/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,RegNr,Model,Seats,Year,Owner,Description,PictureId")] Car car, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Cars.Add(car);
                db.SaveChanges();
                ChangeDataFile(file, x => { car.PictureId = x; db.SaveChanges(); }, null);


                return RedirectToAction("Index");
            }

            ViewBag.Owner = new SelectList(db.AspNetUsers, "Id", "Email", car.Owner);
            return View(car);
        }

        // GET: Cars/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = db.Cars.Find(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            ViewBag.Owner = new SelectList(db.AspNetUsers, "Id", "DisplayName", car.Owner);
            return View(car);
        }

        // POST: Cars/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,RegNr,Model,Seats,Year,Owner,Description,PictureId")] Car car, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(car).State = EntityState.Modified;
                db.Entry(car).Property("PictureId").IsModified = false;
                db.SaveChanges();
                // miski loogika pildi jaoks
                ChangeDataFile(file, x => { car.PictureId = x; db.SaveChanges(); }, car.PictureId);
                // 
                return RedirectToAction("Index");
            }
            ViewBag.Owner = new SelectList(db.AspNetUsers, "Id", "DisplayName", car.Owner);
            return View(car);
        }

        // GET: Cars/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = db.Cars.Find(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(car);
        }

        // POST: Cars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Car car = db.Cars.Find(id);
            int? pictureId = car.PictureId; // jätame enne kustutamist meelde pildi
            db.Cars.Remove(car);
            db.SaveChanges();
            // kontrollime, kui kustatud autol oli pilt, siis kustutame ka selle
            if (pictureId.HasValue)
            {
                db.DataFiles.Remove(
                db.DataFiles.Find(pictureId.Value)
                );
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }


        // lisan siia meetodi juhi lisamiseks autole
        public ActionResult AddDriver(int id, string driverId)
        {
            Car c = db.Cars.Find(id);
            AspNetUser u = db.AspNetUsers.Find(driverId);
            if (c != null && u != null)
            {
                try
                {
                    c.Drivers.Add(u);
                    db.SaveChanges();
                }
                catch (Exception)
                {
                }
            }
            return RedirectToAction("Details", new { id = id });
        }
        public ActionResult RemoveDriver(int id, string driverId)
        {
            Car c = db.Cars.Find(id);
            AspNetUser u = db.AspNetUsers.Find(driverId);
            if (c != null && u != null)
            {
                try
                {
                    c.Drivers.Remove(u);
                    db.SaveChanges();
                }
                catch (Exception)
                {
                }
            }
            return RedirectToAction("Details", new { id = id });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
