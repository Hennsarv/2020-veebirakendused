﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCrakendusDemo.Models;
using System.IO;

namespace MVCrakendusDemo.Controllers
{
    public class MyController : Controller
    {
        protected CarPoolEntities db = new CarPoolEntities();
        //protected northwindEntities2 db = new northwindEntities2();

        [OutputCache(VaryByParam = "*", Location = System.Web.UI.OutputCacheLocation.ServerAndClient, Duration =3600)]
        public ActionResult Content(int id)
        {
            DataFile df = db.DataFiles.Find(id);
            if (df == null) return HttpNotFound();
            return File(df.Content, df.ContentType);
        }

        protected void ChangeDataFile(HttpPostedFileBase file, Action<int> change, int? oldId)
        {
            if (file != null && file.ContentLength > 0)
                using (BinaryReader br = new BinaryReader(file.InputStream))
                {
                    DataFile df = new DataFile
                    {
                        Content = br.ReadBytes(file.ContentLength),
                        Name = file.FileName.Split('\\').Last().Split('/').Last(),
                        ContentType = file.ContentType
                    };
                    db.DataFiles.Add(df);
                    db.SaveChanges();
                    change(df.Id);
                    if (oldId.HasValue)
                    {
                        db.DataFiles.Remove(db.DataFiles.Find(oldId.Value));
                        db.SaveChanges();
                    }

                }
        }



    }
}